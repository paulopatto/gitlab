class IncreaseMrTextColumnSize < ActiveRecord::Migration
  def up
    # MYSQL LARGETEXT for merge request
    #change_column :merge_requests, :st_diffs, :text, :limit => 4294967295
    #change_column :merge_requests, :st_commits, :text, :limit => 4294967295
    #
    #FIX: Correção do erro [ PG::Error: ERROR:  type modifier is not allowed for type "text" ] com o banco de dados Postgresql
    remove_column :merge_requests, :st_diffs
    remove_column :merge_requests, :st_commits

    add_column :merge_requests, :st_diffs,    :text #, :limit => 4294967295
    add_column :merge_requests, :st_commits,  :text #, :limit => 4294967295
  end

  def down
  end
end
